<?php

namespace App\Tests;

use App\Entity\Client;
use PHPUnit\Framework\TestCase;

class UnitTest extends TestCase
{
    public function testTeckup()
    {
        $teckup = new Client();
        $teckup->setClient('teckup');

        $this->assertTrue($teckup->getClient() === 'teckup');
    }
}
